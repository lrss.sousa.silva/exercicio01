const express = require('express');
const app = express();
const port = 8000;

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const secret = "SENHA123"

app.listen(port, () => {
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes', (req, res) => {

    const clientes = [
        {
            id: 1,
            nome: 'Luiza Campos',
            telefone: '11 991803263',
            endereco: 'Rua dos Anjos, 44',
            cpf: '965.233.546-45'
        },
        {
            id: 2,
            nome: 'Alexandre Matos',
            telefone: '11 932450263',
            endereco: 'Avenida um, 236',
            cpf: '974.343.124-35'
        },
        {
            id: 3,
            nome: 'Diana Pontes',
            telefone: '11 12327863',
            endereco: 'Rua dois, 22',
            cpf: '222.222.222-22'
        }
    ]

    res.send(clientes);
});

app.post('/clientes', (req, res) => {
    let valores = req.body;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }

    
    try {
        if (acesso == secret) {
            const novoCliente = {
                nome: valores.nome,
                telefone: valores.telefone,
                endereco: valores.endereco,
                cpf: valores.cpf
                
            }
            httpMessage.statusCode = 201;
            httpMessage.message = 'Cadastrado com sucesso!';
            httpMessage.body = novoCliente
        }
    } catch (error) {

        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})

app.delete('/clientes/:id', (req, res) => {
    let id = req.params.id;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }

    try {
        if (acesso == secret && Number(id)) {
            httpMessage.statusCode = 200;
            httpMessage.message = `Cliente ${id} deletado com sucesso!`;
        }
    } catch (error) {
        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})

app.put('/clientes/:id', (req, res) => {
    let id = req.params.id;
    let valores = req.body;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }

    try {
        const clienteAlterado = {
            id: id,
            nome: valores.nome,
            telefone: valores.telefone,
            endereco: valores.endereco,
            cpf: valores.cpf
        }

        if (acesso == secret && Number(id)) {
            httpMessage.statusCode = 200;
            httpMessage.message = `Usuário ${id} alterado com sucesso!`;
            httpMessage.body = clienteAlterado
        }
    } catch (error) {
        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})

app.get('/funcionarios', (req, res) => {

    const funcionarios = [
        {
            id: 1,
            nome: 'Claudio Rodrigues',
            cpf: '123.345.768-33',
            salario: 1200.55,
            setor: 'Limpeza'
        },
        {
            id: 2,
            nome: 'Larissa Sousa',
            cpf: '245.328.468-44',
            salario: 6000.55,
            setor: 'Tecnologia'
        },
        {
            id: 3,
            nome: 'Mauricio Wender',
            cpf: '263.342.233-99',
            salario: 3000.55,
            setor: 'RH'
        }
    ]

    res.send(funcionarios);
});

app.post('/funcionarios', (req, res) => {
    let valores = req.body;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }
    
    try {
        if (acesso == secret) {
            const novoFuncionario = {
                nome: valores.nome,
                cpf: valores.cpf,
                salario: valores.salario,
                setor: valores.setor
            }
            httpMessage.statusCode = 201;
            httpMessage.message = 'Cadastrado com sucesso!';
            httpMessage.body = novoFuncionario
        }
    } catch (error) {
        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})

app.delete('/funcionarios/:id', (req, res) => {
    let id = req.params.id;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }

    try {
        if (acesso == secret && Number(id)) {
            httpMessage.statusCode = 200;
            httpMessage.message = `Funcionário ${id} deletado com sucesso!`;
        }
    } catch (error) {
        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})

app.put('/funcionarios/:id', (req, res) => {
    let id = req.params.id;
    let valores = req.body;
    let acesso = req.headers['access'];

    const httpMessage = {
        statusCode: "403",
        message: "Acesso insuficiente",
        body: {}
    }

    try {
        const funcionarioAlterado = {
            id: id,
            nome: valores.nome,
            cpf: valores.cpf,
            salario: valores.salario,
            setor: valores.setor
        }

        if (acesso == secret && Number(id)) {
            httpMessage.statusCode = 200;
            httpMessage.message = `Funcionário ${id} alterado com sucesso!`;
            httpMessage.body = funcionarioAlterado
        }
    } catch (error) {
        httpMessage.message = error
        httpMessage.statusCode = error
    }

    res.send(httpMessage)
})